﻿namespace WindowsFormsApplication12
{
    partial class Kalkulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_pow = new System.Windows.Forms.Button();
            this.btn_quit = new System.Windows.Forms.Button();
            this.btn_sqrt = new System.Windows.Forms.Button();
            this.btn_log = new System.Windows.Forms.Button();
            this.btn_cos = new System.Windows.Forms.Button();
            this.btn_sin = new System.Windows.Forms.Button();
            this.btn_podijeljeno = new System.Windows.Forms.Button();
            this.btn_puta = new System.Windows.Forms.Button();
            this.btn_minus = new System.Windows.Forms.Button();
            this.btn_plus = new System.Windows.Forms.Button();
            this.lbl_rezultat = new System.Windows.Forms.Label();
            this.lbl_x = new System.Windows.Forms.Label();
            this.lbl_y = new System.Windows.Forms.Label();
            this.txtB_x = new System.Windows.Forms.TextBox();
            this.txtB_y = new System.Windows.Forms.TextBox();
            this.btn_AC = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_pow
            // 
            this.btn_pow.Location = new System.Drawing.Point(229, 127);
            this.btn_pow.Name = "btn_pow";
            this.btn_pow.Size = new System.Drawing.Size(49, 23);
            this.btn_pow.TabIndex = 0;
            this.btn_pow.Text = "x^2";
            this.btn_pow.UseVisualStyleBackColor = true;
            this.btn_pow.Click += new System.EventHandler(this.btn_pow_Click_1);
            // 
            // btn_quit
            // 
            this.btn_quit.Location = new System.Drawing.Point(122, 200);
            this.btn_quit.Name = "btn_quit";
            this.btn_quit.Size = new System.Drawing.Size(69, 40);
            this.btn_quit.TabIndex = 2;
            this.btn_quit.Text = "Quit";
            this.btn_quit.UseVisualStyleBackColor = true;
            this.btn_quit.Click += new System.EventHandler(this.btn_quit_Click);
            // 
            // btn_sqrt
            // 
            this.btn_sqrt.Location = new System.Drawing.Point(173, 156);
            this.btn_sqrt.Name = "btn_sqrt";
            this.btn_sqrt.Size = new System.Drawing.Size(49, 23);
            this.btn_sqrt.TabIndex = 3;
            this.btn_sqrt.Text = "√";
            this.btn_sqrt.UseVisualStyleBackColor = true;
            this.btn_sqrt.Click += new System.EventHandler(this.btn_sqrt_Click_1);
            // 
            // btn_log
            // 
            this.btn_log.Location = new System.Drawing.Point(173, 127);
            this.btn_log.Name = "btn_log";
            this.btn_log.Size = new System.Drawing.Size(49, 23);
            this.btn_log.TabIndex = 4;
            this.btn_log.Text = "log";
            this.btn_log.UseVisualStyleBackColor = true;
            this.btn_log.Click += new System.EventHandler(this.btn_log_Click_1);
            // 
            // btn_cos
            // 
            this.btn_cos.Location = new System.Drawing.Point(229, 98);
            this.btn_cos.Name = "btn_cos";
            this.btn_cos.Size = new System.Drawing.Size(49, 23);
            this.btn_cos.TabIndex = 5;
            this.btn_cos.Text = "cos";
            this.btn_cos.UseVisualStyleBackColor = true;
            this.btn_cos.Click += new System.EventHandler(this.btn_cos_Click_1);
            // 
            // btn_sin
            // 
            this.btn_sin.Location = new System.Drawing.Point(174, 98);
            this.btn_sin.Name = "btn_sin";
            this.btn_sin.Size = new System.Drawing.Size(49, 23);
            this.btn_sin.TabIndex = 6;
            this.btn_sin.Text = "sin";
            this.btn_sin.UseVisualStyleBackColor = true;
            this.btn_sin.Click += new System.EventHandler(this.btn_sin_Click_1);
            // 
            // btn_podijeljeno
            // 
            this.btn_podijeljeno.Location = new System.Drawing.Point(229, 64);
            this.btn_podijeljeno.Name = "btn_podijeljeno";
            this.btn_podijeljeno.Size = new System.Drawing.Size(49, 23);
            this.btn_podijeljeno.TabIndex = 7;
            this.btn_podijeljeno.Text = "/";
            this.btn_podijeljeno.UseVisualStyleBackColor = true;
            this.btn_podijeljeno.Click += new System.EventHandler(this.btn_podijeljeno_Click_1);
            // 
            // btn_puta
            // 
            this.btn_puta.Location = new System.Drawing.Point(174, 64);
            this.btn_puta.Name = "btn_puta";
            this.btn_puta.Size = new System.Drawing.Size(49, 23);
            this.btn_puta.TabIndex = 8;
            this.btn_puta.Text = "*";
            this.btn_puta.UseVisualStyleBackColor = true;
            this.btn_puta.Click += new System.EventHandler(this.btn_puta_Click_1);
            // 
            // btn_minus
            // 
            this.btn_minus.Location = new System.Drawing.Point(229, 35);
            this.btn_minus.Name = "btn_minus";
            this.btn_minus.Size = new System.Drawing.Size(49, 23);
            this.btn_minus.TabIndex = 9;
            this.btn_minus.Text = "-";
            this.btn_minus.UseVisualStyleBackColor = true;
            this.btn_minus.Click += new System.EventHandler(this.btn_minus_Click_1);
            // 
            // btn_plus
            // 
            this.btn_plus.Location = new System.Drawing.Point(174, 35);
            this.btn_plus.Name = "btn_plus";
            this.btn_plus.Size = new System.Drawing.Size(49, 23);
            this.btn_plus.TabIndex = 10;
            this.btn_plus.Text = "+";
            this.btn_plus.UseVisualStyleBackColor = true;
            this.btn_plus.Click += new System.EventHandler(this.btn_plus_Click_1);
            // 
            // lbl_rezultat
            // 
            this.lbl_rezultat.AutoSize = true;
            this.lbl_rezultat.Location = new System.Drawing.Point(38, 132);
            this.lbl_rezultat.MaximumSize = new System.Drawing.Size(100, 100);
            this.lbl_rezultat.Name = "lbl_rezultat";
            this.lbl_rezultat.Size = new System.Drawing.Size(46, 13);
            this.lbl_rezultat.TabIndex = 11;
            this.lbl_rezultat.Text = "Rezultat";
            // 
            // lbl_x
            // 
            this.lbl_x.AutoSize = true;
            this.lbl_x.Location = new System.Drawing.Point(38, 40);
            this.lbl_x.Name = "lbl_x";
            this.lbl_x.Size = new System.Drawing.Size(14, 13);
            this.lbl_x.TabIndex = 12;
            this.lbl_x.Text = "X";
            // 
            // lbl_y
            // 
            this.lbl_y.AutoSize = true;
            this.lbl_y.Location = new System.Drawing.Point(38, 74);
            this.lbl_y.Name = "lbl_y";
            this.lbl_y.Size = new System.Drawing.Size(14, 13);
            this.lbl_y.TabIndex = 13;
            this.lbl_y.Text = "Y";
            // 
            // txtB_x
            // 
            this.txtB_x.Location = new System.Drawing.Point(90, 40);
            this.txtB_x.Name = "txtB_x";
            this.txtB_x.Size = new System.Drawing.Size(42, 20);
            this.txtB_x.TabIndex = 14;
            // 
            // txtB_y
            // 
            this.txtB_y.Location = new System.Drawing.Point(90, 74);
            this.txtB_y.Name = "txtB_y";
            this.txtB_y.Size = new System.Drawing.Size(42, 20);
            this.txtB_y.TabIndex = 15;
            // 
            // btn_AC
            // 
            this.btn_AC.Location = new System.Drawing.Point(213, 209);
            this.btn_AC.Name = "btn_AC";
            this.btn_AC.Size = new System.Drawing.Size(49, 23);
            this.btn_AC.TabIndex = 16;
            this.btn_AC.Text = "AC";
            this.btn_AC.UseVisualStyleBackColor = true;
            this.btn_AC.Click += new System.EventHandler(this.btn_AC_Click);
            // 
            // Kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_AC);
            this.Controls.Add(this.txtB_y);
            this.Controls.Add(this.txtB_x);
            this.Controls.Add(this.lbl_y);
            this.Controls.Add(this.lbl_x);
            this.Controls.Add(this.lbl_rezultat);
            this.Controls.Add(this.btn_plus);
            this.Controls.Add(this.btn_minus);
            this.Controls.Add(this.btn_puta);
            this.Controls.Add(this.btn_podijeljeno);
            this.Controls.Add(this.btn_sin);
            this.Controls.Add(this.btn_cos);
            this.Controls.Add(this.btn_log);
            this.Controls.Add(this.btn_sqrt);
            this.Controls.Add(this.btn_quit);
            this.Controls.Add(this.btn_pow);
            this.Name = "Kalkulator";
            this.Text = "Kalkulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_pow;
        private System.Windows.Forms.Button btn_quit;
        private System.Windows.Forms.Button btn_sqrt;
        private System.Windows.Forms.Button btn_log;
        private System.Windows.Forms.Button btn_cos;
        private System.Windows.Forms.Button btn_sin;
        private System.Windows.Forms.Button btn_podijeljeno;
        private System.Windows.Forms.Button btn_puta;
        private System.Windows.Forms.Button btn_minus;
        private System.Windows.Forms.Button btn_plus;
        private System.Windows.Forms.Label lbl_rezultat;
        private System.Windows.Forms.Label lbl_x;
        private System.Windows.Forms.Label lbl_y;
        private System.Windows.Forms.TextBox txtB_x;
        private System.Windows.Forms.TextBox txtB_y;
        private System.Windows.Forms.Button btn_AC;
    }
}

