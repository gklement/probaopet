﻿/*Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost znanstvenog kalkulatora, odnosno implementirati
 * osnovne (+,-,*,/) i barem 5 naprednih (sin, cos, log, sqrt...) operacija.*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication12
{
    public partial class Kalkulator : Form
    {
        
        public Kalkulator()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
            private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
       
      
        private void btn_log_Click_1(object sender, EventArgs e)
        {
            double x;
            double.TryParse(txtB_x.Text, out x);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else if (x <= 0)
                MessageBox.Show("X mora biti veci od 0!");
            else
            {
                txtB_y.Clear();
                lbl_rezultat.Text = (Math.Log10(x)).ToString();
            }
        }

        private void btn_pow_Click_1(object sender, EventArgs e)
        {
            double x;
            double.TryParse(txtB_x.Text, out x);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else
            {
                txtB_y.Clear();
                lbl_rezultat.Text = (x * x).ToString();
            }
        }

        private void btn_sqrt_Click_1(object sender, EventArgs e)
        {
            double x;
            double.TryParse(txtB_x.Text, out x);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else if (x < 0) MessageBox.Show("Vrijednost pod korijenom ne smije biti negativna!");
            else
            {
                txtB_y.Clear();
                lbl_rezultat.Text = (Math.Sqrt(x)).ToString();
            }
        }

        private void btn_cos_Click_1(object sender, EventArgs e)
        {
            double x;
            double.TryParse(txtB_x.Text, out x);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else
            {
                txtB_y.Clear();
                lbl_rezultat.Text = (Math.Cos(x * Math.PI / 180)).ToString();
            }
        }

        private void btn_podijeljeno_Click_1(object sender, EventArgs e)
        {
            double x, y;
            double.TryParse(txtB_x.Text, out x);
            double.TryParse(txtB_y.Text, out y);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else if (!double.TryParse(txtB_y.Text, out y))
                MessageBox.Show("Pogresan unos Y!", "Pogreska!");
            else if (y == 0)
                MessageBox.Show("Nazivnik ne smije biti 0!");
            else
            {
                lbl_rezultat.Text = (x / y).ToString();
            }
        }

        private void btn_puta_Click_1(object sender, EventArgs e)
        {
            double x, y;
            double.TryParse(txtB_x.Text, out x);
            double.TryParse(txtB_y.Text, out y);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else if (!double.TryParse(txtB_y.Text, out y))
                MessageBox.Show("Pogresan unos Y!", "Pogreska!");
            else
            {
                lbl_rezultat.Text = (x * y).ToString();
            }
        }

        private void btn_minus_Click_1(object sender, EventArgs e)
        {
            double x, y;
            double.TryParse(txtB_x.Text, out x);
            double.TryParse(txtB_y.Text, out y);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else if (!double.TryParse(txtB_y.Text, out y))
                MessageBox.Show("Pogresan unos Y!", "Pogreska!");
            else
            {
                lbl_rezultat.Text = (x - y).ToString();
            }
        }

        private void btn_plus_Click_1(object sender, EventArgs e)
        {
            double x, y;
            double.TryParse(txtB_x.Text, out x);
            double.TryParse(txtB_y.Text, out y);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else if (!double.TryParse(txtB_y.Text, out y))
                MessageBox.Show("Pogresan unos Y!", "Pogreska!");
            else
            {
                lbl_rezultat.Text = (x + y).ToString();
            }
        }

        private void btn_quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_sin_Click_1(object sender, EventArgs e)
        {
            double x;
            double.TryParse(txtB_x.Text, out x);
            if (!double.TryParse(txtB_x.Text, out x))
                MessageBox.Show("Pogresan unos X!", "Pogreska!");
            else
            {
                txtB_y.Clear();
                lbl_rezultat.Text = (Math.Sin(x * Math.PI / 180)).ToString();
            }
        }

        private void btn_AC_Click(object sender, EventArgs e)
        {
            txtB_x.Clear();
            txtB_y.Clear();
            lbl_rezultat.Text = "Rezultat";
        }

    }
}

       
